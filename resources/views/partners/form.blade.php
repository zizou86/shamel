<!-- Create Provider -->
<div class="card card-default" style="border: 0;">
    <div class="card-header">{{__('Create Provider')}}</div>
    <div class="card-body">
        <form role="form" @submit.prevent>
            <div class="form-group row justify-content-center">
                <div class="col-md-6 d-flex align-items-center">
                    <div class="image-placeholder mr-4">
                        <span v-if="isCreate" role="img" class="profile-photo-preview" :style="previewStyle('')"></span>
                        <span v-if="isUpdate" role="img" class="profile-photo-preview" :style="previewStyle('{{asset('storage/admin/providers/logos')}}/' + provider.logo)"></span>
                    </div>
                    <div class="spark-uploader mr-4">
                        <input ref="photo" type="file" class="spark-uploader-control" name="logo" @change="updateLogo"
                            :disabled="providerForm.busy">
                        <div v-if="isCreate" class="btn btn-outline-dark">{{__('Upload Logo')}}</div>
                        <div v-if="isUpdate" class="btn btn-outline-dark">{{__('Update Logo')}}</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 form-group">
                    <label class="control-label">{{__('Name')}}</label>
                    <input type="text" v-model="providerForm.name" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 form-group">
                    <label class="control-label">{{__('Address')}}</label>
                    <textarea v-model="providerForm.address" class="form-control"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 form-group">
                    <label class="control-label">{{__('Phone')}}</label>
                    <input type="tel" pattern="(05)(5|0|3|6|4|9|1|8|7)([0-9]{7})" v-model="providerForm.phone"
                        class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 form-group">
                <label class="control-label">{{__('Email')}}</label>
                    <input type="email" v-model="providerForm.email" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 form-group">
                    <button class="btn btn-default" v-on:click="toIndex">{{__("Back")}}</button>
                </div>
                <div class="col-lg-6 form-group">
                    <button class="btn btn-success float-right" v-on:click="create">{{__('Create')}}</button>
                </div>
            </div>
        </form>
    </div>
</div>
