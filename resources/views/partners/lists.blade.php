<!-- No Provider List -->
<div class="card card-default" v-if="noProviders">
    <div class="card-header">{{__('Providers list')}}</div>

    <div class="card-body">
        {{__('No providers.')}}
    </div>
</div>

<!-- Provider list -->
<div v-if="! noProviders" class="card card-default">
    <div class="card-header">{{__('Providers list')}}</div>

    <div class="table-responsive">
        <table class="table table-valign-middle mb-0">
            <thead>
                <tr>
                    <th class="th-fit"></th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Email')}}</th>
                    <th>{{__('Phone')}}</th>
                    <th class="th-fit"></th>
                </tr>
            </thead>

            <tbody>
                <tr v-for="provider in providers">
                    <td>
                        <img :src="'{{asset('storage/admin/providers/logos')}}/' + provider.logo" class="spark-profile-photo" alt="{{__('Logo')}}" />
                    </td>
                    <td>
                        <div>
                            @{{ provider.name }}
                        </div>
                    </td>

                    <!-- E-Mail Address -->
                    <td>
                        <div>
                            @{{ provider.email }}
                        </div>
                    </td>

                    <!-- Phone -->
                    <td>
                        <div>
                            @{{ provider.phone }}
                        </div>
                    </td>

                </tr>
            </tbody>
        </table>
    </div>
</div>
