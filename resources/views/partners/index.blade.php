@extends('spark::layouts.app')

@section('content')
<partners inline-template>
    <div class="container">
        <div>
            <div v-if="!isCreate && !isUpdate">
                @include('partners.search')
                <div class="text-right mb-2">
                    <button class="btn btn-success" v-on:click="toCreate">Create</button>
                </div>
                <div v-if="!isSearch">
                    @include('partners.lists')
                </div>
                <div v-else="">
                    @include('partners.results')
                </div>

            </div>
            <div v-else="">
                @include('partners.form')
            </div>
        </div>
    </div>
</partners>
@endsection
